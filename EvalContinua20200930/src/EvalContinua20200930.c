#include "chip.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"


xSemaphoreHandle s1;

xQueueHandle q1;
xQueueHandle q2;

/* Tarea 1 */
static void Task1(void *pvParameters)
{

	uint32_t round_count;

	while(1)
	{
		/* Libero el semáforo contador
		 * de esta manera cada vez que se realiza una
		 * lectura-escritura de datos se decrementa
		 * la cuenta en 1 */
		xSemaphoreGive(s1);

		/* Leo el valor de la cola */
		xQueueReceive(q1, &round_count, portMAX_DELAY);

		/* Incremento el valor */
		round_count++;

		/* Envio el dato la cola */
		xQueueOverwrite(q2, &round_count);
		vTaskDelay(500 / portTICK_RATE_MS);

	}
}


/* Tarea 2 */
static void Task2(void *pvParameters)
{

	uint32_t value;

	while(1)
	{
		/* Leo el valor de la cola */
		xQueuePeek(q2, &value, portMAX_DELAY);

		/* Envio el dato la cola */
		xQueueOverwrite(q1, &value);
	}
}


/* Tarea 3 */
static void Task3(void *pvParameters)
{

	uint32_t value;

	while(1)
	{
		/* Se ingresará a esta tarea sólo cuando
		 * el contador esté en 0, es decir luego
		 * de 5 intercambios de datos */

		/* Tomo el semáforo contador nuevamente 5 veces
		 * para bloquear esta tarea nuevamente luego
		 *  de encender el LED*/

		xSemaphoreTake(s1, portMAX_DELAY);
		xSemaphoreTake(s1, portMAX_DELAY);
		xSemaphoreTake(s1, portMAX_DELAY);
		xSemaphoreTake(s1, portMAX_DELAY);
		xSemaphoreTake(s1, portMAX_DELAY);

		/* Enciendo el led */
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, true);


	}
}




/****************************************************************************************************/
/**************************************** MAIN ******************************************************/
/****************************************************************************************************/

int main(void)
{
	SystemCoreClockUpdate();

	s1 = xSemaphoreCreateCounting(5,5);

	uint32_t initial_value=0;

	q1 = xQueueCreate(1,sizeof(uint32_t));
	xQueueSendToBack(q1, &initial_value, portMAX_DELAY);

	q2 = xQueueCreate(1,sizeof(uint32_t));

	/* Inicio el contador en 5 para luego disminuir en cada paso */
	xSemaphoreTake(s1, portMAX_DELAY);
	xSemaphoreTake(s1, portMAX_DELAY);
	xSemaphoreTake(s1, portMAX_DELAY);
	xSemaphoreTake(s1, portMAX_DELAY);
	xSemaphoreTake(s1, portMAX_DELAY);

	// Led como salida y apagado al inicio
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, false);



    /* Creacion de tareas */
	xTaskCreate(Task1, (char *) "Tarea 1",
    			configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 3UL),
    			(xTaskHandle *) NULL);
    xTaskCreate(Task2, (char *) "Tarea 2",
        		configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
        		(xTaskHandle *) NULL);
    xTaskCreate(Task3, (char *) "Tarea 3",
            	configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 2UL),
            	(xTaskHandle *) NULL);


    /* Start the scheduler */
	vTaskStartScheduler();

	/* Nunca debería arribar aquí */

    return 0;
}

